<?php

namespace Drupal\config_profile\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a setting UI for Config Profile.Inspired by config_ignore module.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_profile.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_profile_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $config_profile_settings = $this->config('config_profile.settings');

    $form['profile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Installation profile'),
      '#description' => $this->t('The installation profile in which you want to export your active configuration.'),
      '#default_value' => $config_profile_settings->get('profile') ?? '',
      '#size' => 60,
    ];

    $form['blacklist'] = [
      '#type' => 'textarea',
      '#rows' => 25,
      '#title' => $this->t('Blacklist of config names that should not be exported to the profile.'),
      '#description' => $this->t('Include one configuration name per line. You can use wildcards (ex. webform.webform.*, block.block.*, etc.).'),
      '#default_value' => $config_profile_settings->get('blacklist') ? implode(PHP_EOL, $config_profile_settings->get('blacklist')) : NULL,
      '#size' => 60,
    ];

    $form['blacklist_property'] = [
      '#type' => 'textarea',
      '#rows' => 25,
      '#title' => $this->t('Blacklist config properties value that should be unset on export to the profile.'),
      '#description' => $this->t('List one configuration name per line with its property, for example: system.site.mail'),
      '#default_value' => $config_profile_settings->get('blacklist_property') ? implode(PHP_EOL, $config_profile_settings->get('blacklist_property')) : NULL,
      '#size' => 60,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config_profile_settings = $this->config('config_profile.settings');
    $blacklist = preg_split("[\n|\r]", $values['blacklist']);
    $blacklist = array_filter($blacklist);
    $blacklist_property = preg_split("[\n|\r]", $values['blacklist_property']);
    $blacklist_property = array_filter($blacklist_property);

    $config_profile_settings->set('blacklist', array_values($blacklist));
    $config_profile_settings->set('blacklist_property', array_values($blacklist_property));
    $config_profile_settings->set('profile', $values['profile']);
    $config_profile_settings->save();
    parent::submitForm($form, $form_state);
  }

}
