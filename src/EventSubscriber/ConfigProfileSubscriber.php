<?php

namespace Drupal\config_profile\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Extension\ProfileExtensionList;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Config event subscriber to copy config to profiles.
 */
final class ConfigProfileSubscriber implements EventSubscriberInterface {

  /**
   * The profile extension list.
   *
   * @var \Drupal\Core\Extension\ProfileExtensionList
   */
  protected ProfileExtensionList $profileList;

  /**
   * The 'config_profile.settings' configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Array containing profile related information.
   *
   * @var array
   */
  protected array $profileInfo = [];

  /**
   * ConfigProfileSubscriber constructor.
   *
   * @param \Drupal\Core\Extension\ProfileExtensionList $profileList
   *   The profile extension list.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ProfileExtensionList $profileList, ConfigFactoryInterface $configFactory) {
    $this->profileList = $profileList;
    $this->config = $configFactory->get('config_profile.settings');
    $this->initializeProfileInfo();
  }

  /**
   * Initialize profile information.
   */
  protected function initializeProfileInfo(): void {
    $profilePath = $this->profileList->getPath($this->config->get('profile'));
    if (!$profilePath) {
      return;
    }
    $this->profileInfo['blacklist'] = $this->config->get('blacklist');
    $this->profileInfo['blacklist_property'] = $this->config->get('blacklist_property');
    $this->profileInfo['profile'] = $this->config->get('profile');
    $this->profileInfo['profile_path'] = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $profilePath;
    $this->profileInfo['profile_config_path'] = $profilePath . '/config/install';
    $this->profileInfo['cleanup'] = FALSE;
    $this->profileInfo['profile_config_files'] = $this->loadProfileConfigFiles($this->profileInfo['profile_config_path']);
  }

  /**
   * Load config files of the profile.
   *
   * @param string $path
   *   The path of the config directory of the profile.
   */
  protected function loadProfileConfigFiles(string $path): array {
    $configFiles = [];
    $directory = new \RecursiveDirectoryIterator($path);
    $iterator = new \RecursiveIteratorIterator($directory);
    $regex = new \RegexIterator($iterator, '/^.+\.yml$/i', \RegexIterator::GET_MATCH);
    foreach ($regex as $file) {
      $configName = basename($file[0], '.yml');
      if (!$this->isBlacklisted($configName)) {
        $configFiles[$configName] = $file[0];
      }
    }
    return $configFiles;
  }

  /**
   * Export transform event for syncing config files to a specific profile.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The config storage transform event.
   */
  public function onExportTransform(StorageTransformEvent $event): void {
    $storage = $event->getStorage();

    if ($this->profileInfo['profile'] && !$storage->getCollectionName()) {
      if (!$this->profileInfo['cleanup']) {
        $this->profileInfo['cleanup'] = TRUE;
        $this->cleanUpConfigDirectory();
      }
    }

    foreach ($storage->listAll() as $name) {
      $data = $storage->read($name);
      unset($data['_core'], $data['uuid']);

      $this->removeBlacklistedProperties($name, $data);

      $yamlData = Yaml::encode($data);

      if (isset($this->profileInfo['profile_config_files'][$name])) {
        // When a config is getting updated from the default location.
        file_put_contents($this->profileInfo['profile_config_files'][$name], $yamlData);
      }
      elseif (!$this->isBlacklisted($name)) {
        // When a new config is getting copied from the default location.
        file_put_contents($this->profileInfo['profile_config_path'] . '/' . $name . '.yml', $yamlData);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [ConfigEvents::STORAGE_TRANSFORM_EXPORT => ['onExportTransform']];
  }

  /**
   * Checks if a config entity should be ignored.
   *
   * @param string $configName
   *   The config entity name.
   *
   * @return bool
   *   Flag checking if a config entity should be ignored.
   */
  protected function isBlacklisted(string $configName): bool {
    return (bool) array_filter($this->profileInfo['blacklist'], fn($pattern) => fnmatch($pattern, $configName));
  }

  /**
   * Cleans up the profile's config directory.
   */
  protected function cleanUpConfigDirectory(): void {
    array_map('unlink', $this->profileInfo['profile_config_files']);
  }

  /**
   * Remove blacklisted properties from specific config.
   *
   * @param string $name
   *   The name of a configuration object to save.
   * @param array $data
   *   The configuration data to filter.
   */
  protected function removeBlacklistedProperties($name, array &$data) {
    $blacklistProperty = $this->profileInfo['blacklist_property'];
    foreach ($blacklistProperty as $blacklistItem) {
      $dataCopy = &$data;
      // Remove the config name from the blacklisted data.
      $item = str_replace($name . ".", "", $blacklistItem, $count);
      if ($count > 0) {
        $keys = explode('.', $item);
        foreach ($keys as $key) {
          // Handle the blacklist config which comes with a property name only.
          if (isset($dataCopy[$key])) {
            if ($key === end($keys)) {
              $dataCopy[$key] = '';
            }
            else {
              $dataCopy = &$dataCopy[$key];
            }
          }
          // Handle the blacklist config which comes with a value.
          elseif (isset($dataCopy) && in_array($key, $dataCopy)) {
            $keyIndex = array_search($key, $dataCopy);
            if ($keyIndex !== FALSE) {
              unset($dataCopy[$keyIndex]);
              // Reindex the array to create YML in the correct format.
              $dataCopy = array_values($dataCopy);
            }
          }
          else {
            // Handle invalid key or structure not found.
            break;
          }
        }
      }
    }
  }

}
